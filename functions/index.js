const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

exports.filter = functions.https.onCall(async (data, context) => {
	let products = [];
	const productsRef = admin
		.firestore()
		.collection("users")
		.doc(context.auth.uid)
		.collection("products");
	if (data.name !== "") {
		const snapshot = await productsRef.where("name", "==", data.name).get();
		if (snapshot.empty) {
			console.log("No matching documents.");
		}
		snapshot.forEach((doc) => {
			products.push({ id: doc.id, ...doc.data() });
		});
	}
	if (data.price !== 0) {
		const snapshot = await productsRef.where("price", "==", data.price).get();
		if (snapshot.empty) {
			console.log("No matching documents.");
		}
		snapshot.forEach((doc) => {
			if (products.indexOf(doc.id) === -1)
				products.push({ id: doc.id, ...doc.data() });
		});
	}
	return products;
});

exports.newUserSignin = functions.auth.user().onCreate((user) => {
	return admin.firestore().collection("users").doc(user.uid).set({
		email: user.email,
		displayName: user.displayName,
		createdAt: new Date(),
	});
});

exports.createProduct = functions.https.onCall((data, context) => {
	if (!context.auth) {
		throw new functions.https.HttpsError(
			"unauthenticated",
			"please login to create a product"
		);
	}
	return admin
		.firestore()
		.collection("users")
		.doc(context.auth.uid)
		.collection("products")
		.add({
			name: data.name,
			price: data.price,
			quantity: data.quantity,
			productPicture: data.productPicture,
		});
});

exports.getProducts = functions.https.onCall(async (data, context) => {
	const snapshot = await admin
		.firestore()
		.collection("users")
		.doc(context.auth.uid)
		.collection("products")
		.get();
	const products = snapshot.docs.map((doc) => {
		return { ...doc.data(), id: doc.id };
	});
	return products;
});

exports.deleteProduct = functions.https.onCall(async (data, context) => {
	if (!context.auth) {
		throw new functions.https.HttpsError(
			"unauthenticated",
			"please login to delete a product"
		);
	}
	return admin
		.firestore()
		.collection("users")
		.doc(context.auth.uid)
		.collection("products")
		.doc(data.productId)
		.delete();
});

exports.getProductDetails = functions.https.onCall(async (data, context) => {
	if (!context.auth) {
		throw new functions.https.HttpsError(
			"unauthenticated",
			"please login to get details of the product"
		);
	}
	const snapshot = admin
		.firestore()
		.collection("users")
		.doc(context.auth.uid)
		.collection("products")
		.doc(data.productId)
		.get();
	return (await snapshot).data();
});

exports.productUpdate = functions.https.onCall(async (data, context) => {
	if (!context.auth) {
		throw new functions.https.HttpsError(
			"unauthenticated",
			"please login to delete a product"
		);
	}
	return admin
		.firestore()
		.collection("users")
		.doc(context.auth.uid)
		.collection("products")
		.doc(data.productId)
		.update({
			name: data.name,
			quantity: data.quantity,
			price: data.price,
			productPicture: data.productPicture,
		});
});
