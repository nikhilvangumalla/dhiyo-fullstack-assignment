# Dhiyo FullStack Assessement

**Note** Operations will be slow because the functions are used many times

Website link https://dhiyo-fullstack-assignment.web.app/

- Auth triggers are used when the user is logging in for the first time a document is created for the user

- All the business logic is done by using as you can see on funciton/index.js file

- Rules are written so that only the authicated user can read or write data to his/her files only. you can see the rules in firestore.rules file
