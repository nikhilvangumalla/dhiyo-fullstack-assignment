import React, { useState, useEffect } from "react";

import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { functions } from "../../lib/utils/firebase";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
	root: {
		"& .MuiFormControl-root": {
			marginBottom: theme.spacing(2),
		},
	},
	center: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%,-50%)",
	},
	loaderContainer: {
		position: "absolute",
		top: "50%",
		left: "50%",
	},
	form: {
		marginTop: theme.spacing(1),
	},
}));

export const ProductCreate = ({ user, history }) => {
	const classes = useStyles();
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);
	const [formData, setFormData] = useState({
		name: "",
		price: 0,
		quantity: 0,
		productPicture: "",
	});

	useEffect(() => {
		if (user) setLoading(false);
	}, [user]);

	if (!user) {
		return (
			<Typography variant="h3" className={classes.center}>
				Please Login to view this page
			</Typography>
		);
	}

	if (loading) {
		return <CircularProgress className={classes.loaderContainer} />;
	}

	if (error) {
		return (
			<Alert severity="error" className={classes.alert}>
				Failed to create Product
			</Alert>
		);
	}
	const handleChange = (field) => (event) => {
		setFormData({ ...formData, [field]: event.target.value });
	};

	const handleSubmit = async (event) => {
		setLoading(true);
		event.preventDefault();
		const createProduct = functions.httpsCallable("createProduct");
		try {
			createProduct(formData)
				.then((result) => {
					console.log(result);
					setLoading(false);
					window.location.href = "/";
				})
				.catch((error) => {
					setLoading(false);
					setError(false);
					throw new Error("failed to create product");
				});
		} catch (error) {}
	};

	return (
		<Container component="main" maxWidth="xs" className={classes.root}>
			<Typography component="h1" variant="h5">
				Create a product
			</Typography>
			<form className={classes.form} onSubmit={handleSubmit}>
				<TextField
					fullWidth
					required
					label="Name"
					id="productName"
					value={formData.name}
					onChange={handleChange("name")}
				/>
				<FormControl fullWidth required>
					<InputLabel htmlFor="price">Amount</InputLabel>
					<Input
						label="Amount"
						id="price"
						value={formData.price}
						type="number"
						onChange={handleChange("price")}
						startAdornment={
							<InputAdornment position="start">&#8377;</InputAdornment>
						}
						inputProps={{ min: "0" }}
					/>
				</FormControl>
				<TextField
					fullWidth
					required
					label="Quantity"
					id="quantity"
					value={formData.quantity}
					type="number"
					onChange={handleChange("quantity")}
					inputProps={{ min: "0" }}
				/>
				<TextField
					fullWidth
					required
					label="Product Picture URL"
					id="productPicture"
					value={formData.productPicture}
					type="url"
					onChange={handleChange("productPicture")}
				/>
				<Button type="submit" variant="contained" color="primary">
					Create
				</Button>
			</form>
		</Container>
	);
};
