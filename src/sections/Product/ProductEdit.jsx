import React, { useState, useEffect } from "react";
import { Typography, CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import { db, functions } from "../../lib/utils/firebase";

const useStyles = makeStyles((theme) => ({
	root: {
		"& .MuiFormControl-root": {
			marginBottom: theme.spacing(2),
		},
	},
	center: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%,-50%)",
	},
	loaderContainer: {
		position: "absolute",
		top: "50%",
		left: "50%",
	},
	form: {
		marginTop: theme.spacing(1),
	},
}));

export const ProductEdit = ({ user, match, history }) => {
	const classes = useStyles();
	const [formData, setFormData] = useState({
		name: "",
		price: 0,
		quantity: 0,
		productPicture: "",
	});
	const [loading, setLoading] = useState(true);
	useEffect(() => {
		if (user) setLoading(false);
		try {
			const getProductDetails = functions.httpsCallable("getProductDetails");
			getProductDetails({ productId: match.params.productId }).then(
				(result) => {
					console.log(result);
					setLoading(false);
					setFormData(result.data);
				}
			);
		} catch (error) {
			console.log("error edit", error);
		}
	}, [user, match]);
	if (!user) {
		return (
			<Typography variant="h3" className={classes.center}>
				Please Login to view this page
			</Typography>
		);
	}
	if (loading) {
		return <CircularProgress className={classes.loaderContainer} />;
	}
	const handleChange = (field) => (event) => {
		setFormData({ ...formData, [field]: event.target.value });
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		try {
			const productUpdate = functions.httpsCallable("productUpdate");
			productUpdate({ productId: match.params.productId, ...formData })
				.then((result) => {
					console.log(result);
					setLoading(false);
					window.location.href = "/";
				})
				.catch((error) => {
					setLoading(false);
					throw new Error("failed to update product");
				});
		} catch (error) {
			console.log("failed to update", error);
		}
	};
	return (
		<Container component="main" maxWidth="xs" className={classes.root}>
			<Typography component="h1" variant="h5">
				Update a product
			</Typography>
			<form className={classes.form} onSubmit={handleSubmit}>
				<TextField
					fullWidth
					required
					label="Name"
					id="productName"
					value={formData.name}
					onChange={handleChange("name")}
				/>
				<FormControl fullWidth required>
					<InputLabel htmlFor="price">Amount</InputLabel>
					<Input
						label="Amount"
						id="price"
						value={formData.price}
						type="number"
						onChange={handleChange("price")}
						startAdornment={
							<InputAdornment position="start">&#8377;</InputAdornment>
						}
						inputProps={{ min: "0" }}
					/>
				</FormControl>
				<TextField
					fullWidth
					required
					label="Quantity"
					id="quantity"
					value={formData.quantity}
					type="number"
					onChange={handleChange("quantity")}
					inputProps={{ min: "0" }}
				/>
				<TextField
					fullWidth
					required
					label="Product Picture URL"
					id="productPicture"
					value={formData.productPicture}
					type="url"
					onChange={handleChange("productPicture")}
				/>
				<Button type="submit" variant="contained" color="primary">
					Update
				</Button>
			</form>
		</Container>
	);
};
