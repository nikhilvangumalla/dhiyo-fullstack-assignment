import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
	Card,
	CardActionArea,
	CardMedia,
	CardContent,
	Grid,
	CardActions,
	TextField,
	Fab,
	Button,
	Typography,
} from "@material-ui/core";
import CreateIcon from "@material-ui/icons/Create";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import clsx from "clsx";
import { functions, signInWithGoogle } from "../../lib/utils/firebase";
import Alert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";
import { ProductSkeleton } from "../../components/ProductSkeleton";

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
	},
	loaderContainer: {
		position: "absolute",
		top: "50%",
		left: "50%",
	},
	media: {
		height: 350,
	},
	marginBottom: {
		marginBottom: 20,
	},
	alert: {
		width: 500,
		margin: "auto",
	},
	form: {
		width: 600,
		margin: "auto",
		marginBottom: 20,
		"& > *": {
			marginRight: 20,
		},
	},
	center: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%,-50%)",
	},
	loginCard: {
		backgroundColor: "#eee",
		minWidth: 600,
		textAlign: "center",
		margin: "auto",
		"& > *": {
			marginTop: 40,
			marginBottom: 20,
		},
	},
	fab: {
		position: "fixed",
		right: 30,
		bottom: 30,
	},
});
export const Home = ({ user, history }) => {
	const classes = useStyles();

	const [products, setProducts] = useState([]);
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);
	const [formData, setFormData] = useState({
		name: "",
		price: 0,
	});

	useEffect(() => {
		if (user) {
			setLoading(true);
			const getProducts = functions.httpsCallable("getProducts");
			getProducts()
				.then((result) => {
					setProducts(result.data);
					setLoading(false);
				})
				.catch((error) => {
					console.log("failed to fetch products", error.message);
					setError(true);
					setLoading(false);
				});
		}
	}, [user]);

	if (loading) {
		return <ProductSkeleton classes={classes} />;
	}

	if (error) {
		return (
			<Alert severity="error" className={classes.alert}>
				Failed to Get Products Please try again
			</Alert>
		);
	}
	const handleLogin = async () => {
		setLoading(true);
		try {
			signInWithGoogle()
				.then((result) => setLoading(false))
				.catch((error) => {
					throw new Error("failed to sign in", error);
				});
		} catch (error) {
			setLoading(false);
			setError(true);
		}
	};

	const handleChange = (field) => (event) => {
		setFormData({ ...formData, [field]: event.target.value });
	};

	const handleSubmit = async (event) => {
		setLoading(true);
		event.preventDefault();
		if (formData.name === "" && formData.price === 0) {
			setProducts(products);
		} else {
			try {
				const filter = functions.httpsCallable("filter");
				filter(formData)
					.then((result) => {
						setProducts(result.data);
						setLoading(false);
					})
					.catch((error) => {
						throw new Error("failed to fiter");
					});
			} catch (error) {
				setLoading(false);
				setError(true);
			}
		}
	};

	const handleDelete = (productId) => {
		setLoading(true);
		const deleteProduct = functions.httpsCallable("deleteProduct");
		deleteProduct({ productId })
			.then((result) => {
				console.log(result);
				setLoading(false);
				window.location.href = "/";
			})
			.catch((error) => console.log(error));
		setLoading(false);
	};

	return (
		<>
			{user ? (
				<>
					<form className={classes.form} onSubmit={handleSubmit}>
						<TextField
							label="Name"
							id="productName"
							value={formData.name}
							onChange={handleChange("name")}
						/>
						<TextField
							label="price"
							id="productName"
							value={formData.price}
							type="number"
							inputProps={{ min: 0 }}
							onChange={handleChange("price")}
						/>
						<Button type="submit" variant="contained" color="primary">
							Filter
						</Button>
					</form>
					<Typography variant="h4" className={classes.heading}>
						Your Products
					</Typography>
					<Grid container spacing={3}>
						{products &&
							products.map((product) => (
								<Grid item xs={12} sm={6} md={4} lg={3} key={product.id}>
									<Card className={classes.root}>
										<CardActionArea>
											<CardMedia
												className={classes.media}
												image={product.productPicture}
												title={`${product.name} Photo`}
											/>
											<CardContent>
												<Typography variant="h5" component="h2">
													{product.name}
												</Typography>
												<Typography variant="h6">
													price: {product.price}
												</Typography>
												<Typography variant="h6">
													quantity: {product.quantity}
												</Typography>
											</CardContent>
										</CardActionArea>
										<CardActions>
											<Link
												to={`/edit/${product.id}`}
												style={{ textDecoration: "none" }}
											>
												<Button
													variant="contained"
													color="primary"
													startIcon={<CreateIcon />}
												>
													Edit
												</Button>
											</Link>
											<Button
												variant="contained"
												color="secondary"
												startIcon={<DeleteIcon />}
												onClick={() => handleDelete(product.id)}
											>
												Delete
											</Button>
										</CardActions>
									</Card>
								</Grid>
							))}
					</Grid>
					<Link to="/create">
						<Fab color="primary" aria-label="add" className={classes.fab}>
							<AddIcon />
						</Fab>
					</Link>
				</>
			) : (
				<Card className={clsx(classes.loginCard, classes.center)}>
					<CardContent className={classes.marginBottom}>
						<Typography variant="h4" className={classes.marginBottom}>
							<span role="img" aria-label="wave">
								👋
							</span>
						</Typography>
						<Typography variant="h4" className={classes.marginBottom}>
							Login to Product Store
						</Typography>
						<Typography component="p" className={classes.marginBottom}>
							sign in with google to view or create your products
						</Typography>
						<Button variant="contained" color="secondary" onClick={handleLogin}>
							Sign In with Google
						</Button>
					</CardContent>
				</Card>
			)}
		</>
	);
};
