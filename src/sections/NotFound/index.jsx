import React from "react";
import { Typography, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles(() => ({
	root: {
		textAlign: "center",
	},
}));

export const NotFound = () => {
	const classes = useStyles();
	return (
		<Container className={classes.root}>
			<Typography variant="h1">404 NotFound</Typography>
		</Container>
	);
};
