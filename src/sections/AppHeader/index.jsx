import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { AppBar, Toolbar, Typography, Button } from "@material-ui/core";
import { auth, signInWithGoogle } from "../../lib/utils/firebase";

const useStyles = makeStyles(() => ({
	root: {
		flexGrow: 1,
		marginBottom: "30px",
	},
	title: {
		flexGrow: 1,
	},
	link: {
		color: "inherit",
		textDecorationLine: "none",
	},
	blur: {
		pointerEvents: "none",
		opacity: 0.4,
	},
}));

export const AppHeader = ({ user, setUser }) => {
	const classes = useStyles();
	const [loading, setLoading] = React.useState(null);
	const handleLogin = async () => {
		setLoading(true);
		await signInWithGoogle();
		setLoading(false);
	};
	return (
		<div className={loading ? classes.blur : ""}>
			<div className={classes.root}>
				<AppBar position="static">
					<Toolbar>
						<Typography variant="h5" className={classes.title}>
							<Link to="/" className={classes.link}>
								Product Store
							</Link>
						</Typography>
						{!user ? (
							<Button style={{ color: "white" }} onClick={handleLogin}>
								<Typography>Login</Typography>
							</Button>
						) : (
							<Button
								style={{ color: "white" }}
								onClick={() => {
									auth.signOut();
									setUser(null);
								}}
							>
								<Typography>Sign Out</Typography>
							</Button>
						)}
					</Toolbar>
				</AppBar>
			</div>
		</div>
	);
};
