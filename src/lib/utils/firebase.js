import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";

const config = {
	apiKey: "AIzaSyCiBwOd67bXizAgSsMsjFbf-aI4Y4J5vgI",
	authDomain: "dhiyo-fullstack-assignment.firebaseapp.com",
	databaseURL: "https://dhiyo-fullstack-assignment.firebaseio.com",
	projectId: "dhiyo-fullstack-assignment",
	storageBucket: "dhiyo-fullstack-assignment.appspot.com",
	messagingSenderId: "569920821034",
	appId: "1:569920821034:web:77b500be5c12fb0de6b053",
};

firebase.initializeApp(config);
export const auth = firebase.auth();
export const db = firebase.firestore();
export const functions = firebase.functions();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({
	prompt: "select_account",
});

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
