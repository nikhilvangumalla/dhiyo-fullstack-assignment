import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";
import Grid from "@material-ui/core/Grid";

export const ProductSkeleton = ({ classes }) => {
	return (
		<>
			<Skeleton
				animation="wave"
				variant="text"
				width={500}
				height={60}
				style={{ margin: "auto", marginBottom: 6 }}
			/>
			<Skeleton
				animation="wave"
				variant="text"
				width={300}
				height={60}
				style={{ marginBottom: 6 }}
			/>
			<Grid container spacing={3}>
				<Grid item xs={12} sm={6} md={4} lg={3}>
					<Skeleton animation="wave" variant="rect" className={classes.media} />
				</Grid>
				<Grid item xs={12} sm={6} md={4} lg={3}>
					<Skeleton animation="wave" variant="rect" className={classes.media} />
				</Grid>
				<Grid item xs={12} sm={6} md={4} lg={3}>
					<Skeleton animation="wave" variant="rect" className={classes.media} />
				</Grid>
				<Grid item xs={12} sm={6} md={4} lg={3}>
					<Skeleton animation="wave" variant="rect" className={classes.media} />
				</Grid>
			</Grid>
		</>
	);
};
