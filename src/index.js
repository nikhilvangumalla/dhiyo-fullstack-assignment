import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom/";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";
import {
	AppHeader,
	Home,
	NotFound,
	ProductCreate,
	ProductEdit,
} from "./sections";

import { auth } from "./lib/utils/firebase";

const useStyles = makeStyles({
	root: {
		maxWidth: 345,
	},
	loaderContainer: {
		position: "absolute",
		top: "50%",
		left: "50%",
	},
});

const App = () => {
	const classes = useStyles();
	const [user, setUser] = useState(null);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		auth.onAuthStateChanged((userAuth) => {
			if (userAuth) {
				setUser(userAuth.uid);
			}
			setLoading(false);
		});
	}, []);

	if (loading) {
		return <CircularProgress className={classes.loaderContainer} />;
	}

	return (
		<Router>
			<AppHeader user={user} setUser={setUser} />
			<Switch>
				<Route
					exact
					path="/"
					render={(props) => <Home {...props} user={user} />}
				/>
				<Route
					exact
					path="/create"
					render={(props) => <ProductCreate {...props} user={user} />}
				/>
				<Route
					exact
					path="/edit/:productId"
					render={(props) => <ProductEdit {...props} user={user} />}
				/>
				<Route component={NotFound} />
			</Switch>
		</Router>
	);
};

render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById("root")
);
